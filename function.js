// 1.)
	const number = 2;
	const exponent = 3;
	getCube = number ** exponent;
	console.log(`The cube of ${number} is ${getCube}.`);


// 2.)
	const address = [258, "Washington Ave.", "NW", "California", 90011];
	const [streetNUmber, streetName, city, country, zipCode] = address;
	console.log(`I  live in ${streetNUmber} ${streetName} ${city}, ${country} ${zipCode}.`);


// 3.)
	const animal = {
		name: "Lolong",
		type: "salwater crocodile",
		weight: 1075,
		measurementft: 20,
		measurementin: 3
	}
	const {name, type, weight, measurementft, measurementin} = animal;
	console.log(`${name} was a ${type}. He weighed at ${weight} kgs. with a measurement of ${measurementft} ft. ${measurementin} in.`);


// 4.)
	const numbers = [1, 2, 3, 4, 5];
	numbers.forEach((number) => console.log(`${number}`));


// 5.))
	const sum = numbers.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
	console.log(sum);


// 6.)
	class Dog
	{
		constructor(name, age, breed)
		{
			this.name = name,
			this.age = age,
			this.breed = breed
		}
	}

	const newDog = new Dog();
	newDog.name = "Frankie";
	newDog.age = 5;
	newDog.breed = "Miniature Dachshund";
	console.log(newDog);